#include <asm/types.h>          /* for videodev2.h */
#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
//#include <sys/time.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <linux/videodev2.h>
#include <dirent.h>
#include "video_capture.h"
#include "h264encoder.h"



extern "C"{

#include "libswscale/swscale.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"

}



#define CLEAR(x) memset (&(x), 0, sizeof (x))

typedef unsigned char uint8_t;



char h264_file_name[100] = "out.h264\0";
FILE *h264_fp;
uint8_t *h264_buf;

char yuv_file_name[100] = "yuv422.yuv\0";


FILE *yuv_fp;

unsigned int n_buffers = 0;
DIR *dirp;
Encoder en;

int cnt = 0;

void errno_exit(const char *s) {
	fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
	exit(EXIT_FAILURE);
}

int xioctl(int fd, int request, void *arg) {
	int r = 0;
	do {
		r = ioctl(fd, request, arg);
	} while (-1 == r && EINTR == errno);

	return r;
}

void open_camera(struct camera *cam) {
	struct stat st;

	if (-1 == stat(cam->device_name, &st)) {
		fprintf(stderr, "Cannot identify '%s': %d, %s\n", cam->device_name,
				errno, strerror(errno));
		exit(EXIT_FAILURE);
	}

	if (!S_ISCHR(st.st_mode)) {
		fprintf(stderr, "%s is no device\n", cam->device_name);
		exit(EXIT_FAILURE);
	}

	cam->fd = open(cam->device_name, O_RDWR, 0); //  | O_NONBLOCK

	if (-1 == cam->fd) {
		fprintf(stderr, "Cannot open '%s': %d, %s\n", cam->device_name, errno,
				strerror(errno));
		exit(EXIT_FAILURE);
	}
}

void close_camera(struct camera *cam) {
	if (-1 == close(cam->fd))
		errno_exit("close");

	cam->fd = -1;
}

void init_file() {
	h264_fp = fopen(h264_file_name, "wb+");
	yuv_fp = fopen(yuv_file_name, "wb+");
}

void close_file() {
	fclose(h264_fp);
	fclose(yuv_fp);
}

void init_encoder(struct camera *cam) {
	compress_begin(&en, cam->width, cam->height);
	h264_buf = (uint8_t *) malloc(
			sizeof(uint8_t) * cam->width * cam->height * 3); // 设置缓冲区
}

void close_encoder() {
	compress_end(&en);
	free(h264_buf);
}
void fferror(int number)
{
//	char buf[100] = { 0 };
//	av_strerror(number, buf, 100);
//	puts(buf);
}

//这是原来的算法，颜色明显不对
int yuv422toyuv422p(uint8_t**out, uint8_t * in ,int size)
{
	int i = 0;

	uint8_t* y = out[0];
	uint8_t* u = out[1];
	uint8_t* v = out[2];


	int pix_type = 0 ;//0 y  1 u  2 y  3 v
	//yu yv yu yv
	for(i = 0; i < size; i++)
	{
		switch (pix_type) {
		case 0:
			*y = in[i];
			y++;
			pix_type = 1;
			break;
		case 1:
			*u = in[i];
			u++;
			pix_type = 2;
			break;
		case 2:
			*y = in[i];
			y++;
			pix_type = 3;
			break;
		case 3:
			*v = in[i];
			v++;
			pix_type = 0;
			break;
			default:
				break;
		}


	}

	return 0;
}

/*
 * 使用ffmpeg的swscale库来转换，简单高效
 */
int yuv422toyuv420p( uint8_t * in ,int size)
{
	//这个是自动缓存的，可以不用管他
	SwsContext *sctx = NULL;
	sctx = sws_getCachedContext(sctx, 640, 480, AV_PIX_FMT_YUYV422, 640,
				480, AV_PIX_FMT_YUV420P, SWS_BICUBIC, 0, 0, 0);


	static bool init = false;

	static AVFrame *yuv =NULL;

	static uint8_t *xdata[AV_NUM_DATA_POINTERS] = {0};
	static	int xlinesize[AV_NUM_DATA_POINTERS] = {0};

	if(!init)
	{
		init = true;
		yuv = av_frame_alloc();
		yuv->format = AV_PIX_FMT_YUV420P;
		yuv->width = 640;
		yuv->height = 480;
		//yuv->pict_type = AV_PICTURE_TYPE_I;
		 av_frame_get_buffer(yuv, 32);



		 xdata[0] = (uint8_t *)malloc(640 * 480);
		 xdata[1] = (uint8_t *)malloc(640 * 480 / 4);
		 xdata[2] = (uint8_t *)malloc(640 * 480 / 4);

		 xlinesize[0] = 640;
		 xlinesize[1] = 320;
		 xlinesize[2] = 320;

	}





	static uint8_t * outimg[8] = { 0 };

		//yuv420在h264中用的肯定是panler格式啦
	if (outimg[0] == NULL)
		outimg[0] = (uint8_t*) malloc(640*480*3);
	if (outimg[1] == NULL)
		outimg[1] = (uint8_t*) malloc(640*480);
	if (outimg[2] == NULL)
		outimg[2] = (uint8_t*) malloc(640*480);

	int outLineSize[8] = { 0 };

	outLineSize[0] = 640;
	outLineSize[1] = 640;
	outLineSize[2] = 640;



	uint8_t * srcimg[8] = {0};
	srcimg[0] = in;
	int srcLineSize[8] = { 0 };
	srcLineSize[0] = 640 * 2;




	int h = sws_scale(sctx, srcimg, srcLineSize, 0, 480, xdata, xlinesize);

	printf("%d %d %d %d\n",yuv->linesize[0],yuv->linesize[1],yuv->linesize[2],yuv->linesize[3]);


	static FILE*fp = NULL;
	if(fp == NULL)
		fp=fopen("yuv420.yuv", "wb+");

	int ret;
	if(h > 0)
	{
		ret = fwrite(xdata[0], 640*480, 1, fp);


		ret= fwrite(xdata[1], 640*480 / 4, 1, fp);
		ret = fwrite(xdata[2],640*480 / 4, 1, fp);

	}


	return h;
}

void encode_frame(uint8_t *yuv_frame, size_t yuv_length) {
	int h264_length = 0;

	//这里有一个问题，通过测试发现前6帧都是0，所以这里我跳过了为0的帧
	if (yuv_frame[0] == '\0')
		return;





	h264_length = compress_frame(&en, -1, yuv_frame, h264_buf);
	if (h264_length > 0) {
		//写h264文件
		fwrite(h264_buf, h264_length, 1, h264_fp);
	}

	//写yuv422文件,摄像头读出来的
	fwrite(yuv_frame, yuv_length, 1, yuv_fp);
	//printf("<%d>",yuv_length);







}

int read_and_encode_frame(struct camera *cam) {
	struct v4l2_buffer buf;

	printf("in read_frame\n");

	CLEAR(buf);

	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;

	//this operator below will change buf.index and (0 <= buf.index <= 3)
	if (-1 == xioctl(cam->fd, VIDIOC_DQBUF, &buf)) {
		switch (errno) {
		case EAGAIN:
			return 0;
		case EIO:
			/* Could ignore EIO, see spec. */
			/* fall through */
		default:
			errno_exit("VIDIOC_DQBUF");
		}
	}

	encode_frame((uint8_t*)cam->buffers[buf.index].start, buf.length);

	if (-1 == xioctl(cam->fd, VIDIOC_QBUF, &buf))
		errno_exit("VIDIOC_QBUF");

	return 1;
}

void start_capturing(struct camera *cam) {
	unsigned int i;
	enum v4l2_buf_type type;

	for (i = 0; i < n_buffers; ++i) {
		struct v4l2_buffer buf;

		CLEAR(buf);

		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = i;

		if (-1 == xioctl(cam->fd, VIDIOC_QBUF, &buf))
			errno_exit("VIDIOC_QBUF");
	}

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (-1 == xioctl(cam->fd, VIDIOC_STREAMON, &type))
		errno_exit("VIDIOC_STREAMON");

}

void stop_capturing(struct camera *cam) {
	enum v4l2_buf_type type;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (-1 == xioctl(cam->fd, VIDIOC_STREAMOFF, &type))
		errno_exit("VIDIOC_STREAMOFF");

}
void uninit_camera(struct camera *cam) {
	unsigned int i;

	for (i = 0; i < n_buffers; ++i)
		if (-1 == munmap(cam->buffers[i].start, cam->buffers[i].length))
			errno_exit("munmap");

	free(cam->buffers);
}

void init_mmap(struct camera *cam) {
	struct v4l2_requestbuffers req;

	CLEAR(req);

	req.count = 4;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	//分配内存
	if (-1 == xioctl(cam->fd, VIDIOC_REQBUFS, &req)) {
		if (EINVAL == errno) {
			fprintf(stderr, "%s does not support "
					"memory mapping\n", cam->device_name);
			exit(EXIT_FAILURE);
		} else {
			errno_exit("VIDIOC_REQBUFS");
		}
	}

	if (req.count < 2) {
		fprintf(stderr, "Insufficient buffer memory on %s\n", cam->device_name);
		exit(EXIT_FAILURE);
	}

	cam->buffers = (struct buffer *)calloc(req.count, sizeof(*(cam->buffers)));

	if (!cam->buffers) {
		fprintf(stderr, "Out of memory\n");
		exit(EXIT_FAILURE);
	}

	for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
		struct v4l2_buffer buf;

		CLEAR(buf);

		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = n_buffers;

		//将VIDIOC_REQBUFS中分配的数据缓存转换成物理地址
		if (-1 == xioctl(cam->fd, VIDIOC_QUERYBUF, &buf))
			errno_exit("VIDIOC_QUERYBUF");

		cam->buffers[n_buffers].length = buf.length;
		cam->buffers[n_buffers].start = mmap(NULL /* start anywhere */,
				buf.length, PROT_READ | PROT_WRITE /* required */,
				MAP_SHARED /* recommended */, cam->fd, buf.m.offset);

		if (MAP_FAILED == cam->buffers[n_buffers].start)
			errno_exit("mmap");
	}
}

void init_camera(struct camera *cam) {
	struct v4l2_capability *cap = &(cam->v4l2_cap);
	struct v4l2_cropcap *cropcap = &(cam->v4l2_cropcap);
	struct v4l2_crop *crop = &(cam->crop);
	struct v4l2_format *fmt = &(cam->v4l2_fmt);
	unsigned int min;

	if (-1 == xioctl(cam->fd, VIDIOC_QUERYCAP, cap)) {
		if (EINVAL == errno) {
			fprintf(stderr, "%s is no V4L2 device\n", cam->device_name);
			exit(EXIT_FAILURE);
		} else {
			errno_exit("VIDIOC_QUERYCAP");
		}
	}

	if (!(cap->capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		fprintf(stderr, "%s is no video capture device\n", cam->device_name);
		exit(EXIT_FAILURE);
	}

	if (!(cap->capabilities & V4L2_CAP_STREAMING)) {
		fprintf(stderr, "%s does not support streaming i/o\n",
				cam->device_name);
		exit(EXIT_FAILURE);
	}

	//#ifdef DEBUG_CAM
	printf("\nVIDOOC_QUERYCAP\n");
	printf("the camera driver is %s\n", cap->driver);
	printf("the camera card is %s\n", cap->card);
	printf("the camera bus info is %s\n", cap->bus_info);
	printf("the version is %d\n", cap->version);
	//#endif
	/* Select video input, video standard and tune here. */

	CLEAR(*cropcap);

	cropcap->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	crop->c.width = cam->width;
	crop->c.height = cam->height;
	crop->c.left = 0;
	crop->c.top = 0;
	crop->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	CLEAR(*fmt);

	fmt->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt->fmt.pix.width = cam->width;
	fmt->fmt.pix.height = cam->height;
	fmt->fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV; //yuv422
	//  fmt->fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420  //yuv420 但是我电脑不支持
	fmt->fmt.pix.field = V4L2_FIELD_INTERLACED; //隔行扫描

	if (-1 == xioctl(cam->fd, VIDIOC_S_FMT, fmt))
		errno_exit("VIDIOC_S_FMT");

	/* Note VIDIOC_S_FMT may change width and height. */

	/* Buggy driver paranoia. */
	min = fmt->fmt.pix.width * 2;
	if (fmt->fmt.pix.bytesperline < min)
		fmt->fmt.pix.bytesperline = min;
	min = fmt->fmt.pix.bytesperline * fmt->fmt.pix.height;
	if (fmt->fmt.pix.sizeimage < min)
		fmt->fmt.pix.sizeimage = min;

	init_mmap(cam);

}

void v4l2_init(struct camera *cam) {
	open_camera(cam);
	init_camera(cam);
	start_capturing(cam);
	init_encoder(cam);
	init_file();
}

void v4l2_close(struct camera *cam) {
	stop_capturing(cam);
	uninit_camera(cam);
	close_camera(cam);
	free(cam);
	close_file();
	close_encoder();
}

